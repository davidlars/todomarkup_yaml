Install jq and yq  
Run:  
$> ./commands/actionable_tasks.sh todo.yaml # etc  

Treat the yaml-file as the source.  
yq '.issues[] | select(.priority==1)' todo.yaml  
# funkar inte? yq -y '.issues[] | {id: .id, title: .title, prio: .priority, status: .status}' todo.yaml  
yq -o json todo.yaml|jq '.issues[] | {id: .id, title: .title, prio: .priority, status: .status}' -  

# show open issues  
yq '.issues.[] | select(.status == "open")' todo.yaml  
# show titles of open issues  
yq '.issues.[] | select(.status == "open") | .title' todo.yaml   

# filter on multiple properties  
 yq '.issues.[] | select(.status == "open") | select(.tags == "*food*") | select(.context == "home")' todo.yaml  

# open issues, print certain fields  
yq -o json '.issues.[] | select(.status == "open")' todo.yaml |jq '{title: .title, prio: .priority, status: .status}' -  
