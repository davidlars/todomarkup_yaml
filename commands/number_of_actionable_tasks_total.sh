#!/bin/bash
set -e
if [ $# -eq 0 ]; then
  echo "Usage:"
  echo "[COMMAND] FILENAME.yaml"
  exit 1
fi
cat $1|yq|jq '.tasks | .[] | {title: .title, actionable: .actionable}'|grep -B1 "yes"|grep "title"|wc -l|xargs
