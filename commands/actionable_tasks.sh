#!/bin/bash
set -e
if [ $# -eq 0 ]; then
  echo "Usage:"
  echo "[COMMAND] FILENAME.yaml"
  exit 1
fi
cat $1|yq|jq '.tasks | map(select(.actionable == ("yes")))'|jq '.[] | .title'
