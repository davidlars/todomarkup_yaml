#!/bin/bash
set -e
if [ $# -eq 0 ]; then
  echo "Usage:"
  echo "[COMMAND] FILENAME.yaml"
  exit 1
fi
cat $1|yq|jq '.tasks | .[] | {title: .title, steps: .steps}'|jq '.title, [.steps|length]'|grep -B2 1|grep '"'|cut -d' ' -f 1-10
