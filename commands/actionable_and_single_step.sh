#!/bin/bash
set -e
if [ $# -eq 0 ]; then
  echo "Usage:"
  echo "[COMMAND] FILENAME.yaml"
  exit 1
fi
comm  -1 -2 <(./commands/actionable_tasks.sh $1 |sort) <(./commands/single_step_tasks.sh $1 |sort)
