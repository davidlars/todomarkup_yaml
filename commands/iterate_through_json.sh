#!/bin/bash
set -e
while read task; do
    ACTIONABLE=$(jq -r '.actionable' <<< $task | grep yes)
    if [ "no" != "yessss" ]; then
      echo $ACTIONABLE
      jq -r '.actionable' <<< $task
    fi
done <<< $(jq -c '.tasks[]' todo.json)
